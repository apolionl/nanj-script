## Script

This script in theory, shoud calculate the [sigma-profile][1] and [sigma potential][1] [(ensemble)][2] for a set of data  
( *charge | area | temperature* ).  

Please use `python script.py -v ` for obtaining more information this script.  

It is important to mention that the output generated for this script has Markdown syntax.  
Therefore, the output can be easy and cleanly visualised preview in some web-browers,   
jupyter lab, and some text editors.

```
python script.py -inp my_only_data.dat |  tee formated-output.md 
```

Here an example of the output.


 Segment |    Pot. density
     --- | ---
       1 |    10.871547
       2 |    10.851572
       3 |    10.893366
       4 |    10.992406
       5 |    10.866148
       6 |    10.976949
       7 |    11.026278
       8 |    10.853205
       9 |    10.897450
      10 |    10.857766


---


## Licence

MIT 


[1]: <https://www.annualreviews.org/doi/full/10.1146/annurev-chembioeng-073009-100903> "Review paper"
[2]: <http://www.cosmologic.de/files/downloads/theory/COSMO-RS-Theory-Basics.pdf> "Graphical principle p:7 "
