#!/usr/bin/python3

import os               # access to system folders
import pandas as pd     # tables
import numpy as np      # fast arrays
import argparse         # parsing commands
import sys              # kill script

#  Parsing help, unput file, and sort summany about the script
parser = argparse.ArgumentParser()
parser.add_argument("-v", "--verbose", help="information about the script",
                    action="store_true")
parser.add_argument("-inp", dest="file_path", required=False,
                    help="name of the file with inputdata")
args = parser.parse_args()


if args.verbose:
    print("""
    Sript to calculate chemical potential density based on at least
    10 segments (rows) of formated input data containing surface
    charge densities, area, and temperature.
    > charge-1 | charge-2 | area | temperature

    By default reads input data from "surface-charge.dat". If file is
    not found, or has less that 10 segments, the script generates
    random values in order to perform the calculation. Own input file
    can be provided using "-inp file_name" .

    usage:
          python name_this_script [-h] [-v] [-inp FILE_PATH]

    """)
    sys.exit()


#  A Python class for controling better the input and out put. This change and
#  the main, should make easier to use this script as a module in the future.
class Chem_Pot_Density:

    #  Define constansts
    E_permit = 0.127440612281609  # e^2/kcal/mol *A
    a_eff = 7.4                   # A^2
    k_bt = 0.001985875            # kcal/mol/Å
    min_tol = 0.001               # Convergence criterion
    min_seg = 10                  # minimum number of segments

    def __init__(self):
        self.name = ""
        self.input = pd.DataFrame()
        self.Einter = np.array([])
        self.CPD = np.array([])

    def input_name(self):
        print("# Chemical potential density calculator ")
        if args.file_path:
            self.name = args.file_path
            print(f" - Using file: \t {self.name}")
        else:
            self.name = "surface-charge.dat"
            print(f" - Using file: \t {self.name}")

    def input_check(self):
        """
        Function that checks input data and defines several variables. It also
        creates namely dummy data in case not data is provided.

        """
        #  Check that the file exists and has at least 10 rows
        check_file = os.path.isfile(self.name)
        if check_file:
            self.input = pd.read_table(self.name, skipinitialspace=True,
                                       comment='#', delim_whitespace=True,
                                       header=None)
            if len(self.input) > self.min_seg-1:
                print(f" - File has {len(self.input)} rows \n")
                self.input.set_axis(["charge",  "area", "tempt"], axis=1,
                                    inplace=True)
            else:
                print("""
                Script requires a file with at least `10 rows` creating
                a surface-charge.dat file. Provided data saved in backup as
                file_name-extention.bak

                """)
                np.savetxt((self.name+".bak"), self.input.values, fmt='%.10e',
                           comments="#\tbackup\n #\tcharge \tarea \ttempt",
                           header="\t")
                self.create_data()
        #  If does not exist, create random data
        else:
            print(" - File does not exist in the current directory (",
                  os.getcwd(), "), \n", "creating new surface-charge.dat\n")
            self.create_data()

    def create_data(self):
        """
        Creates random surface charge data points in order to
        use subsequently in the chemical potential density calculation. Created
        data is saved in current directory as surface-charge.dat.

        """
        self.input = pd.DataFrame({"charge": np.random.uniform(-1., 1., size=(self.min_seg)),
                                   "area": np.random.uniform(10.5, 225.5, size=(self.min_seg)),
                                   "tempt": pd.Series(np.random.uniform(250.15, 325.15),
                                                      index=list(range(self.min_seg)), dtype='float64')})
        np.savetxt("surface-charge.dat", self.input.values, fmt='%.10e',
                   comments="#\tcharge \tarea \ttempt", header="\t")

    def inter_energy(self):
        """
        Calculates the interaction energy for a pair of charge densities over
        two segments of surface. Input must be a pair.

        """
        data = self.input.charge.to_numpy()
        for i in np.nditer(data):
            for j in np.nditer(data):
                self.Einter = np.append(self.Einter, (0.3*pow(self.a_eff, 3/2))/(2*self.E_permit)*pow(i+j, 2))
        #  Constructs the symmetry matrix from interaction energies
        self.Einter = np.reshape(self.Einter, (data.size, data.size))
        #  Creates the chemical potential density vector
        self.CPD = np.zeros(data.size)

    def calc_pot_density(self):
        """
        Compute the chemical potential density per segment, using as input
        pre-computed interaction energies from all possible pair interactions.

        """
        #  Array containing factor in function of temperature.
        temp_const = np.array([-self.k_bt*self.input.at[uni_temp, "tempt"]/self.a_eff
                               for uni_temp in range(len(self.input.index))])
        #  Probability as defined in book "Encyclopedia of Physical Organic
        #  Chemistry , 6 Volume Set" This characterise the ensemble.
        #  Array containing probability for each segments.
        sigm_prof = np.array([self.input.at[uni_area, "area"]/self.input.area.sum()
                              for uni_area in range(len(self.input.index))])
        condition = None
        chem_pot_dens = self.CPD.copy()
        itera = 1
        #  time.sleep(0.5)
        #  print(f"This is the CPD {round(chem_pot_dens, 3)} ")
        #  Starts ensemble U_s of interacting pairs of surface segments
        while not condition:
            pre_chem_pot_dens = chem_pot_dens.copy()
            for CPD in range(chem_pot_dens.size):
                intg_term = np.array([sigm_prof[CPD] * np.exp((1/temp_const[CPD]) * (Einter-chem_pot_dens[CPD]))
                                      for Einter in self.Einter[np.triu_indices(chem_pot_dens.size, 1)]])
                #  Calculate part of the integral within the equation 2,
                #  and change its value in the array.
                chem_pot_dens[CPD] = temp_const[CPD] * np.log(np.sum(intg_term))
            #  To the recently calculated chem_pot_dens, it is added a bit of the previos
            #  chem_pot_dens. This trick helped to make converge the chem_pot_dens.
            #  The method used in the original paper is used here as well.
            chem_pot_dens = (chem_pot_dens + pre_chem_pot_dens)/2
            rmse = np.square(np.subtract(chem_pot_dens, pre_chem_pot_dens)).mean()
            if rmse > self.min_tol:
                condition = False
            else:
                condition = True
            itera += 1
            if condition is True or itera > 99999:
                break
        self.CPD = chem_pot_dens.copy()

    def export_data(self):
        """
        Save computed chemical potential density results in to a file in the
        current directory.

        """
        #  Save calculated values as a file
        np.savetxt((self.name)+"-results.dat", self.CPD, fmt='%.10e',
                   comments="#\t chemical potential densities [kcal/mol/Å²]",
                   header="\t")

    def print_result(self):
        """
        Print formated table of chemical potential densities.

        """
        print("### Converged chemical potential densities [kcal/mol/Å²] \n\n",
              "Segment | ", "  Pot. density\n", "    --- | ---")
        for index, potential in enumerate(np.nditer(self.CPD), start=1):
            print("{:8} |    {:12.8f}".format(index, potential))
        print("> Results have been saved as chemmical-potential-dens.dat",
              "\n> in the current directory. (", os.getcwd(), ") \n")


if __name__ == "__main__":
    system = Chem_Pot_Density()
    system.input_name()
    system.input_check()
    system.inter_energy()
    system.calc_pot_density()
    system.export_data()
    system.print_result()
